import pytest
from hcsr04 import HCSR04


@pytest.mark.parametrize('threshold,samples,filtered', [
    (0.5, [20, 21, 20.1, 20.01], [21]),
    (0.01, [20, 21, 20.1, 20.01], [21, 20.1, 20.01]),
    (1.7, [20, 21, 20.1, 20.01], []),
    (1, [23, 23.9, 80, 22.1, 22.6, 23.7, 29], [80, 29]),
    (1, [23, 23.9, 80, 22.1, 22.6, 23.7, 29, 28.7, 28.8, 28.9, 28.2, 29.1],
     [23, 23.9, 80, 22.1, 22.6, 23.7]),
])
def test_absolute_error_thresholding(threshold, samples, filtered):
    obj = HCSR04(absolute_error_threshold=threshold)

    n_usable, values = obj.filter_measurements(samples)
    assert n_usable == len(samples) - len(filtered)
    for value in values:
        assert value not in filtered
        assert value in samples


@pytest.mark.parametrize('threshold,samples,filtered', [
    (50, [20, 21, 20.1, 20.01], []),
    (1.5, [20, 21, 20.1, 20.01], [21]),
    (31, [20, 21, 20.1, 20.01], []),
])
def test_relative_error_thresholding(threshold, samples, filtered):
    obj = HCSR04(relative_error_threshold=threshold)

    n_usable, values = obj.filter_measurements(samples)
    assert n_usable == len(samples) - len(filtered)
    for value in values:
        assert value not in filtered
        assert value in samples
