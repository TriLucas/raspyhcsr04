"""Interface for HC-SR04 distance sensors.

This module provides classes for controlling ultrasonic distance sensors
of the type HC-SR04. You can choose between a polling and a threading approach,
or you can extend L{HCSR04} to define custom behavior.

The provided classes are:
    - HCSR04
    - HCSR04Polling
    - HCSR04Threaded

"""
from functools import wraps

from datetime import datetime
from time import sleep

from threading import Semaphore
from threading import Condition

from .config import SensorConfig

from .logger import LOGGER


__all__ = ['HCSR04', 'HCSR04Polling', 'HCSR04Threaded']


def needs_initialized(func):
    @wraps(func)
    def wrapper(obj, *args, **kwargs):
        if not obj.is_initialized():
            raise obj.NotInitializedError()
        return func(obj, *args, **kwargs)

    return wrapper


class HCSR04:
    """Abstract base class for HC-SR04 controlling classes."""

    # https://cdn.sparkfun.com/datasheets/Sensors/Proximity/HCSR04.pdf
    _MIN_DIST = 2  # Minimal measurable distance in cm
    _MAX_DIST = 400  # Maximal measurable distance in cm

    _TRIGGER_PERIOD = 0.00001  # Specified HIGH time of the trigger pin

    @classmethod
    def from_config(cls, sensor_name, filename='sensors.ini'):
        """
        Create a sensor controller object from a configuration file.

        See L{config.SensorConfig} for a description on how to write
        configuration files.

        @param sensor_name: The Section in which the desired sensor is defined.
        @param filename: The configuration file. (default sensors.ini)
        @return: A sensor controller object.
        """
        conf = SensorConfig(cls)
        conf.read(filename)
        return cls(**conf.sensor_params(sensor_name))

    def __init__(self, backend=None, average_of: int = 5, max_tries: int = 10,
                 absolute_error_threshold: float = None,
                 relative_error_threshold: float = None,
                 air_temperature: float = 20, timeout: float = None,
                 float_precision: int = None,
                 cycle_length: float = 0.06,
                 keep_cycle_length: bool = True):
        """
        Construct a HCSR04 object.

        @param backend: IO backend instance.
        @param average_of: Amount of actual measurements per measurement.
            (default 5)
        @param max_tries: Maximum amount of measurement tries. (default 10)
        @param absolute_error_threshold: Minimum relative variance at which a
            measurement is considered erroneous in cm. Ignored when
            B{relative_error_threshold} is defined. (default None)
        @param relative_error_threshold: Minimum absolute variance at which a
            measurement is considered erroneous in %. (default None)
        @param air_temperature: The air's temperature the sensor is operated in
            (in celsius). (default 20)
        @param timeout: Time (in seconds) after which a measurement should be
            considered unsuccessful. When not specified, the timeout is set to
            two times the time the acoustic signal would take to travel the
            sensors highest reliably measurable distance (400cm).
        @param float_precision: The precision, to which every result of a
            floating point operation should be rounded. (default None)
        @param cycle_length: The time (in seconds) that continuous measurements
            should be delayed. (default 0.06)
        @param keep_cycle_length: Whether or not continuous measurements should
            be delayed to after the suggested times of previous measurements.
            (default 60ms)
        """
        self._backend = backend

        self._cycle_length = cycle_length
        self._keep_cycle_length = keep_cycle_length in ['1', 'true', True, 1]
        self._float_precision = float_precision

        self._air_temperature = air_temperature

        self._timeout = timeout or (self._MAX_DIST * 4 / self.sound_speed_cm())

        self._absolute_error_threshold = absolute_error_threshold
        self._relative_error_threshold = relative_error_threshold

        self._repetitions = average_of
        self._max_tries = max_tries

        self._initialized = False
        self._threshold_map = None

        self._gpio_semaphore = Semaphore()
        self._on_measure_start = None

    def set_on_measure_start(self, callback):
        """Set an arbitrary callable, executed before each measurement."""
        self._on_measure_start = callback

    def now(self):
        """@return: the current time as a timestamp in seconds."""
        return datetime.now().timestamp()

    @property
    def echo(self):
        """@return: The state {0,1} of the configured echo input port."""
        with self._gpio_semaphore:
            return self._backend.read_echo()

    def __enter__(self):
        """
        Context manager entry point for sensor controller objects.

        Ensures the backend is properly initialized before performing any
        operation.

        @return: the sensor controller object.
        """
        self.initialize()

        return self

    def __exit__(self, exc_type, exc_value, exc_tb):
        """
        Context manager exit point for sensor controller objects.

        Ensures the backend is properly cleaned up.
        """
        self.cleanup()

    def is_initialized(self):
        """
        Check whether the backend was properly initialized.

        @return: A boolean determining the backends initialization status.
        """
        return self._initialized

    @needs_initialized
    def measure(self, keep_details=False):
        """
        Perform a measurement according to the object's configuration.

        Perform measurements until the desired amount of measurements to build
        an average from was properly executed, calculate and return the
        average result.

        @param: keep_details: Whether or not a detailed list of all taken
                              samples should also be returned.

        @return: The distance between the controlled sensor and the physical
            object it's facing ( + a detailed list of all measured samples if
            desired).
        """
        values = []
        attempts = 0

        if self._threshold_map is not None:
            del self._threshold_map
            self._threshold_map = None

        not_enough = True
        n_usable = None
        usable_values = None

        next_start = self.now()

        if keep_details:
            details = []

        while not_enough and attempts < self._max_tries:
            attempts += 1

            if self._keep_cycle_length:
                remaining = self._cycle_length - (self.now() - next_start)
                if remaining > 0:
                    sleep(remaining)

            try:
                if self._on_measure_start is not None:
                    self._on_measure_start()

                distance, has_reset = self._measure()
                values.append(distance)
                if keep_details:
                    details.append((distance, has_reset))
            except self.MeasurementError as e:
                LOGGER.warning(e)

            next_start = self.now()

            n_usable, usable_values = self.filter_measurements(values)
            not_enough = n_usable < self._repetitions

        if not_enough:
            raise self.MeasurementError(
                f'Not enough usable measurements: {n_usable}',
            )

        result = self._weighted_average(n_usable, usable_values)
        if keep_details:
            return result, details
        return result

    def sound_speed_m(self):
        """
        Get the speed of sound at the configured air temperature (m/s).

        @return: The speed of sound in m/s.
        """
        # https://en.wikipedia.org/wiki/Speed_of_sound#Practical_formula_for_dry_air
        return 331.3 + 0.606 * self._air_temperature

    def sound_speed_cm(self):
        """
        Get the speed of sound at the configured air temperature (cm/s).

        @return: The speed of sound in cm/s.
        """
        return self.sound_speed_m() * 100

    def echo_distance(self, start, end):
        """
        Calculate an acoustic signal's travelled distance.

        @param start: Starting time of the acoustic signal (in seconds).
        @param end: Ending time of the acoustic signal (in seconds).
        @return: The distance the acoustic signal travelled (in cm).
        """
        diff = end - start
        if diff >= self._timeout:
            raise self.TimeoutError()

        distance = self._float((diff * self.sound_speed_cm()) / 2)
        if not self._MIN_DIST < distance < self._MAX_DIST:
            raise self.RangeError(f'Impossible distance: {distance}')

        return distance

    def filter_measurements(self, measurements):
        """
        Filter a list of measurements by the configured variance thresholds.

        Filter a list of measurements by this object's configured variance
        thresholds. When no variance thresholds are defined, the input list's
        contents are returned.

        @param measurements: List of unfiltered measurements.
        @return: A tuple containing the maximum length of filtered elements and
            a generator for these elements.
        """
        relative = self._relative_error_threshold is not None
        absolute = self._absolute_error_threshold is not None
        if not relative and not absolute:
            return len(measurements), measurements

        def _relative(reference, checked):
            res = abs(checked / reference * 100 - 100)
            LOGGER.warning(f'Variance: {res:.5f}')
            return res

        def _absolute(reference, checked):
            return abs(reference - checked)

        if relative and absolute:
            LOGGER.warning(f'Absolute ({self._absolute_error_threshold}) + ' +
                           f'relative ({self._relative_error_threshold}) ' +
                           'threshold specified. Relative threshold ignored.')

        if self._threshold_map is None:
            self._threshold_map = {'max': -1, 'max_ref_idx': -1, 'map': {}}

        fnc = _relative
        threshold = self._relative_error_threshold

        if absolute:
            fnc = _absolute
            threshold = self._absolute_error_threshold

        # When actually filtering by a threshold, the (first) value for which
        # the most samples satisfy the configured threshold shall be the
        # reference value. We need to test every given value.
        for ref_idx, ref_val in enumerate(measurements):
            self._threshold_map['map'].setdefault(
                ref_idx,
                {'count': 0, 'indexes': {}},
            )

            _mapped = self._threshold_map['map'][ref_idx]
            for check_idx, check_val in enumerate(measurements):
                if check_idx in _mapped['indexes']:
                    continue

                satisfying = fnc(ref_val, check_val) < threshold
                _mapped['indexes'][check_idx] = satisfying
                if satisfying:
                    cnt = _mapped['count'] + 1
                    if cnt > self._threshold_map['max']:
                        self._threshold_map['max_ref_idx'] = ref_idx
                        self._threshold_map['max'] = cnt
                    _mapped['count'] = cnt

        return self._threshold_map['max'], self._yield_filtered(measurements)

    def _yield_filtered(self, measurements):
        ref_idx = self._threshold_map['max_ref_idx']
        references = self._threshold_map['map'][ref_idx]['indexes']
        for check_idx, satisfying in references.items():
            if satisfying:
                yield measurements[check_idx]

    def _float(self, value):
        if self._float_precision is None:
            return value
        return round(value, self._float_precision)

    def _measure(self):
        raise NotImplementedError

    def initialize(self):
        """Initialize the sensor controller object's backend."""
        self._initialized = True
        self._backend.initialize_backend()

    def cleanup(self):
        """Clean the sensor controller object's backend up."""
        self._initialized = False
        self._backend.cleanup_backend()

    def _weighted_average(self, n_values, values):
        values_map = {}

        result = 0

        for value in values:
            values_map.setdefault(value, 0)
            values_map[value] += 1

        for value, occurrences in values_map.items():
            result += value * (occurrences / n_values)

        return self._float(result)

    @needs_initialized
    def _trigger_sensing(self):
        self._backend.set_trigger(True)
        sleep(self._TRIGGER_PERIOD)
        self._backend.set_trigger(False)

    class MeasurementError(Exception):
        """Representation of a general measurement error."""

    class TimeoutError(MeasurementError):
        """Representation of a measurement timeout error."""

        def __init__(self, *args, **kwargs):
            """Construct the timeout error."""
            super().__init__('Timeout during measurement.', *args, **kwargs)

    class RangeError(MeasurementError):
        """Representation of a measured impossible distance."""

    class NotInitializedError(Exception):
        """Representation of a measured impossible distance."""


class HCSR04Polling(HCSR04):
    """
    HC-SR04 class abstraction with a polling strategy.

    Implements L{HCSR04} to block during measurements, until every measurement
    is finished.
    """

    @needs_initialized
    def _measure(self):
        self._trigger_sensing()
        start = echo_start = end = self.now()

        has_reset = False

        while self.echo == 0 and self.now() - start < self._timeout:
            has_reset = True
            echo_start = self.now()

        while self.echo == 1 and self.now() - echo_start < self._timeout:
            pass
        end = self.now()

        return self.echo_distance(echo_start, end), has_reset


class HCSR04Threaded(HCSR04):
    """
    HC-SR04 class abstraction with a threaded callback strategy.

    Implements L{HCSR04} to block during measurements, until every measurement
    is finished. The configured input port's state changes trigger callbacks,
    which notify a thread to process the measured times.
    """

    def __init__(self, *args, **kwargs):
        """Construct a HCSR04Threaded object."""
        super().__init__(*args, **kwargs)
        self._last_exception = None
        self._last_distance = None

        self._measuring = False
        self._measure_start = None
        self._measure_end = None

        self._access = Semaphore()

        self._echo_rise_callback = None
        self._echo_fall_callback = None

        self._end_condition = Condition()

    @property
    def echo_rise_callback(self):
        if self._echo_rise_callback is None:
            self._echo_rise_callback = self._get_rising_callback()
        return self._echo_rise_callback

    @property
    def echo_fall_callback(self):
        if self._echo_fall_callback is None:
            self._echo_fall_callback = self._get_falling_callback()
        return self._echo_fall_callback

    @needs_initialized
    def _get_rising_callback(self):

        def callback(*args):
            with self._access:
                if self._measuring:
                    self._measure_start = self.now()
        return callback

    @needs_initialized
    def _get_falling_callback(self):

        def callback(*args):
            notifier = None
            with self._access:
                if self._measuring and self._measure_start is not None:
                    self._measure_end = self.now()
                    self._measuring = False

                    notifier = self._end_condition

            if notifier is not None:
                with notifier:
                    notifier.notify_all()
        return callback

    def initialize(self):
        super().initialize()
        self._backend.set_callbacks(self.echo_rise_callback,
                                    self.echo_fall_callback)

    @needs_initialized
    def _measure(self):
        with self._access:
            self._measure_start = None
            self._measure_end = None

            self._measuring = True

        self._trigger_sensing()

        with self._end_condition:
            self._end_condition.wait(self._timeout)

        with self._access:
            if self._measuring or self._measure_end is None:
                self._measuring = False
                raise self.TimeoutError()

        return (
            self.echo_distance(self._measure_start, self._measure_end),
            False,
        )
