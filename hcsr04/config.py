"""Custom parser for L{HCSR04} configuration files.

This module provides a specialized ConfigParser class for L{HCSR04} specific
configuration files. These configuration files are supposed to provide a
versatile way of defining sensor controller objects, especially (but not
exclusively) their backends.

The provided class is:
    - SensorConfig
"""

from configparser import ConfigParser
import importlib
import inspect
import os

from .logger import LOGGER


class SensorConfig(ConfigParser):
    """
    L{HCSR04} specialization of configparser.ConfigParser.

    A configuration file is written in the INI format, where the sections
    specify the sensor's name (i.e. we select a single sensor's configuration
    by reading the key/value pairs in the section name like the sensor).

    The parameters handled by this specialized ConfigParser class equal the
    parameters for
    L{HCSR04's (+ subclasses) constructor<hcsr04.HCSR04.__init__>}.
    Additionally, the parameters for the
    L{BaseBackend constructor<hcsr04.backends.BaseBackend.__init__>} are also
    handled, as well as parameters specific to constructors of subclasses of
    L{BaseBackend<hcsr04.backends.BaseBackend>}. The parameters types are
    determined (converted if necessary) by parameter annotations.

    Note about the B{backend} parameter:
    May have a python module (e.g. backends.dummy) or a relative or absolute
    path to a python source file assigned. The specified module must provide a
    B{BACKEND_CLASS} attribute, which is instantiated with configured
    parameters during parsing.

    Example sensors.ini file::
        [Sensor1]
        backend = backends.dummy
        backend_chip = backends.dummy.CHIP_ID
        average_of = 5

    Example for a custom annotated backend parameter::
        # file: backends/sqrt.py
        ...
        def _make_sqrt(value):
            return math.sqrt(value)

        class RootBackend(BaseBackend):
            def __init__(self, exponential: _make_sqrt, *args, **kwargs)
                self._value
                ...
        BACKEND_CLASS = RootBackend

        # file: sensors.ini
        [SqrtSensor]
        backend = backends.sqrt
        exponential = 4
        trigger_address = 100

    In the above example the sensor control object's backend would be an
    instance of B{RootBackend} with its attribute B{_value} set to 2 by
    applying B{_make_sqrt}.

    B{The configuration file location}

    During instantiation, a B{sensors.ini} file is searched in the following
    locations (in order):
        - In the current working directory
        - An absolute or relative path provided by the environment variable
          B{SENSORS_CONFIG}
        - In the directories listed and ordered by the I{U{XDG Base Directory
          Specification
          <https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html>}}
    When no configuration file could be found, any one method of
    U{configparser.ConfigParser
    <https://docs.python.org/3/library/configparser.html>}
    must be used to manually load a file.
    """

    def _read_default_locations(self):
        locations = [
            os.environ.get('SENSORS_CONFIG', None),
            os.path.abspath(os.path.curdir),
            os.environ.get('XDG_CONFIG_HOME', '$HOME/.config'),
            os.environ.get('XDG_CONFIG_DIRS', '/etc/xdg'),
        ]

        real_locations = [os.path.expanduser(os.path.expandvars(path))
                          for path in locations if path is not None]

        for location in real_locations:
            test_path = location
            if not test_path.endswith('.ini'):
                test_path = os.path.join(location, 'sensors.ini')

            if os.path.exists(test_path) and os.path.isfile(test_path):
                LOGGER.info(f'Reading from {test_path}.')
                self.read(test_path)
                break

    def __init__(self, target_class, *args, **kwargs):
        """Instantiate a SensorConfig object."""
        super().__init__(*args, **kwargs)
        self.__read = False
        self._target_class = target_class

        self._read_default_locations()

    def _get_backend_object(self, sensor_name, param):
        backend_name = self[sensor_name][param]
        backend_module = importlib.import_module(
            backend_name,
            'hcsr04' if backend_name.startswith('.') else None,
        )

        backend_class = backend_module.BACKEND_CLASS

        return backend_module.BACKEND_CLASS(
            **self._configured_params(sensor_name, backend_class),
        )

    _SPECIAL_MAPPING = {
        'backend': _get_backend_object,
    }

    _TYPE_ANNOTATION_MAPPING = {
        int: 'getint',
        float: 'getfloat',
        bool: 'getboolean',
    }

    def _args_and_types(self, fnc):
        arg_spec = inspect.getfullargspec(fnc)
        return {arg: arg_spec.annotations.get(arg, None)
                for arg in arg_spec.args if arg not in ['cls', 'self']}

    def _get_class_constructor_params(self, klass):
        result = self._args_and_types(klass.__init__)

        for parent in klass.__bases__:
            result.update(self._args_and_types(parent.__init__))

        return result

    def _configured_params(self, sensor_name, klass):
        result = {}

        target_args = self._get_class_constructor_params(klass)
        present_special_params = set(target_args.keys()).intersection(
            set(self._SPECIAL_MAPPING.keys()),
        )

        for arg in present_special_params:
            target_args.pop(arg)
            result[arg] = self._SPECIAL_MAPPING[arg](self, sensor_name, arg)

        for arg, _type in target_args.items():
            getter = getattr(self[sensor_name],
                             self._TYPE_ANNOTATION_MAPPING.get(_type, 'get'))
            value = getter(arg)
            if value is not None:
                if _type is not None:
                    value = _type(value)

                result[arg] = value

        return result

    def sensor_params(self, sensor_name):
        """Parse the sensors configuration into a key/value collection."""
        return self._configured_params(sensor_name, self._target_class)

    def make_sensor(self, sensor_name):
        """Create a sensor control object from the configuration."""
        return self._target_class(**self.sensor_params(sensor_name))
