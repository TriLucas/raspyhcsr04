from . import BaseBackend


class DummyBackend(BaseBackend):
    def __init__(self, distance=10.0, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._distance = distance

    def initialize_backend(self, callback=None, threaded=False):
        return

    def cleanup_backend(self):
        return

    def set_trigger(self):
        return

    def read_echo(self):
        return 1


BACKEND_CLASS = DummyBackend
