from . import BaseBackend
from RPi import GPIO

from ..logger import LOGGER


def _mode_from_config(value):
    module, attribute = value.split('.')
    if module != 'GPIO':
        raise ValueError('Parameter "mode" must be specified like GPIO.<MODE>')
    return getattr(GPIO, attribute)


class RaspberryPi(BaseBackend):
    def __init__(self, mode: _mode_from_config, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if isinstance(mode, str):
            if mode.startswith('GPIO.'):
                self._mode = getattr(GPIO, mode.split('.')[1])
        else:
            self._mode = mode

    def _input_channel(self):
        return [
            channel for channel in [self._echo_address,
                                    self._echo_address_rising,
                                    self._echo_address_falling]
            if channel is not None
        ]

    def initialize_backend(self):
        GPIO.setmode(self._mode)

        GPIO.setup(self._trigger_address, GPIO.OUT)

        for channel in self._input_channel():
            LOGGER.info(f'Setting up {channel} as an input.')
            GPIO.setup(channel, GPIO.IN)

        self.clear_callback()

    def set_callbacks(self, for_rising=None, for_falling=None, for_both=None):
        self.clear_callback()

        if for_rising is not None:
            channel = self._echo_address_rising or self._echo_address
            GPIO.add_event_detect(channel, GPIO.RISING, for_rising)

        if for_falling is not None:
            channel = self._echo_address_falling or self._echo_address
            GPIO.add_event_detect(channel, GPIO.FALLING, for_falling)

        if for_both is not None:
            for channel in self._input_channel():
                GPIO.add_event_detect(channel, GPIO.BOTH, for_both)

    def clear_callback(self):
        for channel in self._input_channel():
            GPIO.remove_event_detect(channel)

    def cleanup_backend(self):
        self.clear_callback()
        GPIO.cleanup(self._input_channel() + [self._trigger_address])

    def set_trigger(self, value=False):
        GPIO.output(self._trigger_address, value)

    def read_echo(self):
        return GPIO.input(self._echo_address)


BACKEND_CLASS = RaspberryPi
