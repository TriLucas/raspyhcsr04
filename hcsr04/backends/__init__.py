class BaseBackend:
    def __init__(self, echo_address: int = None, trigger_address: int = None,
                 echo_address_rising: int = None,
                 echo_address_falling: int = None):
        self._echo_address = echo_address
        self._echo_address_rising = echo_address_rising
        self._echo_address_falling = echo_address_falling
        self._trigger_address = trigger_address

    def initialize_backend(self):
        raise NotImplementedError

    def set_callbacks(self, for_rising, for_falling, for_both):
        raise NotImplementedError

    def clear_callback(self):
        raise NotImplementedError

    def cleanup_backend(self):
        raise NotImplementedError

    def set_trigger(self):
        raise NotImplementedError

    def read_echo(self):
        raise NotImplementedError
