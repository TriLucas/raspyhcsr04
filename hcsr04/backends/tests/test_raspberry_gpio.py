import pytest
from unittest import mock

from threading import Thread
from threading import Semaphore

from time import sleep
from datetime import datetime

import random

random.seed(datetime.now().timestamp())


class TimedGPIOMock(mock.Mock):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._to_rising_edge = 0
        self._to_falling_edge = 0

    def now(self):
        return datetime.now().timestamp()

    def set_time_to_edges(self, to_rising, to_falling):
        self._to_rising_edge = to_rising
        self._to_falling_edge = to_falling


class ThreadedGPIOMock(TimedGPIOMock):
    FALLING = 'falling'
    RISING = 'rising'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._callbacks = {}
        self._active_timer_thread = None

        self._next = 0
        self._signal_semaphore = Semaphore()

    def get_next(self):
        with self._signal_semaphore:
            return self._next

    def set_next(self, value):
        with self._signal_semaphore:
            self._next = value

    def input(self, *args, **kwargs):
        return self.get_next()

    def add_event_detect(self, port, directions, callback):
        self._callbacks[directions] = callback

    def _wait_for_mimi_thread(self, timeout):
        self._active_timer_thread.join(timeout)

    def set_active_sensor(self, instance):
        _condition_wait = instance._end_condition.wait

        def _proxy_wait(timeout):
            self._wait_for_mimi_thread(timeout)
            return _condition_wait(timeout)

        instance._end_condition.wait = _proxy_wait

    def output(self, channel, value):
        if value:
            return

        def _mimic_sensor():
            self.set_next(0)
            t_first = self.now()

            self._callbacks[self.FALLING]()

            self.set_next(1)
            t_second = self.now()
            if t_second - t_first < self._to_rising_edge:
                sleep(self._to_rising_edge - (t_second - t_first))
            t_third = self.now()

            self._callbacks[self.RISING]()

            self.set_next(0)
            t_fourth = self.now()
            if t_fourth - t_third < self._to_falling_edge:
                sleep(self._to_falling_edge - (t_fourth - t_third))

            self._callbacks[self.FALLING]()

        self._active_timer_thread = Thread(target=_mimic_sensor)
        self._active_timer_thread.start()


class PollingGPIOMock(TimedGPIOMock):
    # Mocking the Raspberry Pi specific RPi.GPIO module, i.e. the bits we need
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._timer = 0
        self._sensor_active = False
        self._has_risen = False

    def input(self, *args, **kwargs):
        if not self._sensor_active:
            return 0
        passed = self.now() - self._timer
        if passed <= self._to_rising_edge:
            return 0
        elif (passed <= self._to_falling_edge + self._to_rising_edge
              or not self._has_risen):
            self._has_risen = True
            return 1

        self._sensor_active = False
        return 0

    def output(self, channel, value):
        if value:
            return

        self._timer = self.now()
        self._sensor_active = True
        self._has_risen = False


@pytest.fixture(scope='module')
def gpio_mock_polling():
    return PollingGPIOMock()


@pytest.fixture(scope='module')
def gpio_mock_threaded():
    return ThreadedGPIOMock()


@pytest.fixture(scope='module')
def gpio_polling_patch(gpio_mock_polling):
    module_mock = mock.MagicMock(GPIO=gpio_mock_polling)
    return mock.patch.dict('sys.modules', **{'RPi': module_mock})


@pytest.fixture(scope='module')
def gpio_threaded_patch(gpio_mock_threaded):
    module_mock = mock.MagicMock(GPIO=gpio_mock_threaded)
    return mock.patch.dict('sys.modules', **{'RPi': module_mock})


MAX_ERROR = 0.3

# Boundaries for tested echo travel distances / times.
TESTING_RANGES_CM = (343.42, 51.513)
TESTING_RANGES_MS = (19.1, 3.9)


def _sensor_method_error(sensor, to_rising, to_falling):
    manual = sensor.echo_distance(to_rising, to_falling)

    measured = sensor.measure()
    err = abs(manual - measured) / manual
    assert err < MAX_ERROR
    assert TESTING_RANGES_CM[0] > measured
    assert measured > TESTING_RANGES_CM[1]


def _random_in_range(max_value, min_value, resolution, amount):
    return [
        (random.randint(1, resolution) / resolution) * (max_value - min_value)
        + min_value for x in range(amount)
    ]


@pytest.fixture(scope='session')
def sensor_config(tmpdir_factory):
    ini = """
[Sensor1]
average_of = 2
max_tries = 3
backend = .backends.raspberryGPIO
keep_cycle_length = false
mode = GPIO.BCM
trigger_address = 12
echo_address_rising = 16
echo_address_falling = 20
"""
    config_dir = tmpdir_factory.mktemp('configs')
    filename = config_dir.join('sensors.ini')
    filename.write(ini)
    return str(filename)


@pytest.mark.xfail(raises=AssertionError)  # Thread timings are not easy to debug
@pytest.mark.parametrize('to_rising', [0.02, 0.03, 0.01])
@pytest.mark.parametrize('to_falling', _random_in_range(*TESTING_RANGES_MS,
                                                        2000, 10))
def test_polling_error_x_10(gpio_mock_polling, gpio_polling_patch, to_rising,
                            to_falling, sensor_config):
    to_rising /= 1000
    to_falling /= 1000

    gpio_mock_polling.set_time_to_edges(to_rising, to_falling)
    with gpio_polling_patch:
        from hcsr04 import HCSR04Polling
        with HCSR04Polling.from_config('Sensor1', sensor_config) as sensor:
            _sensor_method_error(sensor, to_rising, to_falling)


@pytest.mark.xfail(raises=AssertionError)  # Thread timings are not easy to debug
@pytest.mark.parametrize('to_rising', [0.02, 0.03, 0.01])
@pytest.mark.parametrize('to_falling', _random_in_range(*TESTING_RANGES_MS,
                                                        2000, 10))
def test_threaded_error_x_10(gpio_mock_threaded, gpio_threaded_patch,
                             to_rising, to_falling, sensor_config):
    to_rising /= 1000
    to_falling /= 1000

    gpio_mock_threaded.set_time_to_edges(to_rising, to_falling)
    with gpio_threaded_patch:
        from hcsr04 import HCSR04Threaded
        with HCSR04Threaded.from_config('Sensor1', sensor_config) as sensor:
            gpio_mock_threaded.set_active_sensor(sensor)
            _sensor_method_error(sensor, to_rising, to_falling)
