"""Logging facility for the sensor controllers.

This module provides a logging Class for outputting arbitrary log messages.
The provided object is 'LOGGER'
"""
import logging

LOGGER = logging.getLogger('HC-SR04')

LOGGER.setLevel(logging.ERROR)
handler = logging.StreamHandler()
LOGGER.addHandler(handler)

__all__ = ['LOGGER']
