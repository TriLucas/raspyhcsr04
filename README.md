# RasPyHCSR04

A module for controlling HC-SR04 type sensors. This module should only be used
on compatible machines (i.e. [Raspberry Pi][1])

## Links:
* [API Documentation][2]
* [Publications on test.pypi.org][3]

[1]: https://www.raspberrypi.org/
[2]: https://trilucas.gitlab.io/raspyhcsr04/documentation/
[3]: https://test.pypi.org/project/RasPyHCSR04/
