import subprocess
import argparse

import configparser

from setuptools import setup


def _cmd_out_str(cmd):
    return subprocess.check_output(cmd).strip().decode()


def get_git_version():
    try:
        tag = _cmd_out_str(['git', 'describe', '--tags', '--abbrev=0'])
    except subprocess.CalledProcessError:
        tag = ''

    if not tag:
        n_cmd = ['git', 'rev-list', 'HEAD', '--count']
    else:
        n_cmd = ['git', 'rev-list', f'{tag}..HEAD', '--count']

    n_commits = _cmd_out_str(n_cmd)

    if tag:
        return f'{tag}.{n_commits}'
    return f'0.dev{n_commits}'


def get_version_string_write_cfg():
    parser = configparser.ConfigParser()
    try:
        git_version = get_git_version()
    except subprocess.CalledProcessError:
        git_version = None

    if not parser.read('setup.cfg'):
        parser.add_section('metadata')

    if git_version is not None:
        parser['metadata']['version'] = git_version
        with open('setup.cfg', 'w') as fp:
            parser.write(fp)

    return parser['metadata']['version']


META = {
    'name': 'RasPyHCSR04',
    'version': get_version_string_write_cfg(),
    'author': 'Tristan Lucas',
    'author_email': 'tristan.lucas.de@gmail.com',
    'description': 'Library for accessing the HC-SR04 distance sensors',
    'long_description': open('README.md').read(),
    'long_description_content_type': 'text/markdown',
    'url': 'https://gitlab.com/TriLucas/raspyhcsr04.git',
    'project_urls': {
        'Bug Tracker': 'https://gitlab.com/TriLucas/raspyhcsr04/issues',
    },
    'classifiers': [
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: Other OS',
        'Topic :: Scientific/Engineering',
    ],
    'packages': ['hcsr04', 'hcsr04.backends'],
    'python_requires': '>=3.6',
    'install_requires': 'RPi.GPIO ==0.7.0; "arm" in platform_machine',
}


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--make-version', action='store_true', default=False,
                        required=False)

    args = parser.parse_known_args()
    if args[0].make_version:
        print(get_version_string_write_cfg())
    else:
        setup(**META)
